
function htmlToElement(html) {
  var template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

function block(newItem, event) {
  if (event && event.keyCode !== 13) {
      return;
  }

  if (newItem.trim().length < 3) return;

  chrome.storage.sync.get(['blocked'], function(items) {
    chrome.storage.sync.set({'blocked': [...items.blocked, {type: 'custom', term: newItem.trim()}]});
    generateList();
    updateTabs();
  });


  document.getElementById('block-term').value = '';
}

function remove(term, id) {
  console.log(term);

  chrome.storage.sync.get(['blocked'], function(items) {
    for (let i = 0; i < items.blocked.length; i++) {
      if (items.blocked[i].term === term) {
        console.log('fff')
        items.blocked.splice(i, 1);
        chrome.storage.sync.set({'blocked': items.blocked});
        document.getElementById(id).remove();
        updateTabs();
        return;
      }
    }
  });
}

document.getElementById('block').onclick = () => block(document.getElementById('block-term').value);
document.getElementById('block-term').onkeyup = (e) => block(document.getElementById('block-term').value, e);


function compare(a, b) {
  if (a.term < b.term) {
    return -1;
  } else {
    return 1;
  }
}


let customTerms = document.getElementById('custom-terms');
let titleTerms = document.getElementById('title-terms');
let channelTerms = document.getElementById('channel-terms');
let element;
generateList = () => {
  chrome.storage.sync.get(['blocked'], function(items) {
    if (!items.blocked) {
      chrome.storage.sync.set({'blocked': []});
    }

    items.blocked.sort(compare);

    customTerms.innerHTML = '';
    titleTerms.innerHTML = '';
    channelTerms.innerHTML = '';

    for (let i = 0; i < items.blocked.length; i++) {
      if (items.blocked[i].type === 'custom') {
        element = customTerms;
      } else if (items.blocked[i].type === 'title') {
        element = titleTerms;
      } else {
        element = channelTerms;
      }

      let id = 'id' + Math.round(Math.random() * 100000);
  
      element.appendChild(htmlToElement(`
        <div class="item" id='${id}'>
          - ${items.blocked[i].term}
        </div>
      `));
  
      document.getElementById(id).onclick = () => remove(items.blocked[i].term, id);
    }
  });
}

function updateTabs() {
  chrome.tabs.query({}, function(tabs) {
    for (let i = 0; i < tabs.length; i++) {
      console.log(tabs[i]);
      if (tabs[i].url.indexOf('youtube.com') !== -1) {
        chrome.tabs.sendMessage(tabs[i].id, {action: "update"});
      }
    }

  });
  generateList();
}

let thanks = false;

function addDonationButton(sku, price, currency) {
  if (thanks) {
    thankYou();
    return;
  }

  if (currency === "EUR") {
    currency = "&euro;";
  } else if (currency === "GBP") {
    currency = "£";
  } else if (currency === "USD" || currency === "AUD" || currency === "NZD") {
    currency = "$";
  } else {
    currency = currency + " ";
  }

  let label;
  if (sku === "donation") {
    label = `Donate ${currency}${price} Once-Off`;
  } else if (sku === "donation_recurring2") {
    label = `Donate ${currency}${price} Monthly`;
  } else if (sku === "donation_recurring") {
    return;
  }

  let buttons = document.getElementById('button-holder');

  buttons.appendChild(htmlToElement(`
    <button id="${sku}">
      ${label}
    </button>
  `));

  document.getElementById(sku).onclick = () => {
    chrome.runtime.sendMessage({msg: "buy", sku: sku});
  };
}

function thankYou(sku) {
  thanks = true;
  document.getElementById('thank-you').classList.remove('hidden');
  document.getElementById('donation-holder').classList.add('hidden');
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.msg === "add_items") {
      addDonationButton(request.data.sku, request.data.price, request.data.currency);
    }
    if (request.msg === "has_bought") {
      thankYou(request.sku);
    }
  }
);

chrome.runtime.sendMessage({msg: "request_items"});
chrome.runtime.sendMessage({msg: "check_buy"});
generateList();
