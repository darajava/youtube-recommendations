
// Save it using the Chrome extension storage API.
// chrome.storage.sync.set({'blocked': ['chess', 'jordan peterson']});
// chrome.storage.sync.clear();

function remove(newItem, elem, type) {
  chrome.storage.sync.get(['blocked'], function(items) {
    if (items.blocked) {
      chrome.storage.sync.set({'blocked': [...items.blocked, {type: type, term: newItem.trim()}]});
    } else {
      chrome.storage.sync.set({'blocked': [{type: type, term: newItem}]});
    }
    
    if (!paused) {
      elem.addClass('fade-out-crx');
    
      setTimeout(cleanup, 600);      
    } else {
      cleanup();
    }

    $('.prompt-crx').remove();
  });
}

function promptRemove(title, channel, elem, id) {
  $('.prompt-crx').remove();

  elem.append(`
    <div
      class="prompt-crx"
    >
      <div class="close-crx" id="close-${id}">×</div>
      <button id="title-${id}">Never recommend this video again</button>
      <button id="channel-${id}">Never recommend any videos from channel: ${channel}</button>
    </div>
  `);

  document.getElementById('close-' + id).onclick = () => $('.prompt-crx').remove();
  document.getElementById('title-' + id).onclick = () => remove(title, elem, 'title');
  document.getElementById('channel-' + id).onclick = () => remove(channel, elem, 'channel');

}

let oldUrl;

function cleanup(caller) {
  if (document.location.href !== oldUrl) {
    oldUrl = document.location.href;
    paused = false;
    $('.remove-crx').remove();
    $('.prompt-crx').remove();
  }

  $('.yt-next-continuation').off('click.crx').on('click.crx', () => {
    setTimeout(() => cleanup('first1'), 1000)
    setTimeout(() => cleanup('second1'), 3000)
  });

  chrome.storage.sync.get(['blocked'], function(items) {
    let elements = $('ytd-grid-video-renderer, ytd-compact-video-renderer');


    elements.each(function() {
      let title = $(this).find('#video-title').text().trim();
      let channel = $(this).find('#byline').text().trim();
    
      console.log('elements', title);

      let id = (title + channel).replace(/[\W_]+/g,"");
      
      if ($(this).find('.remove-crx').attr('id') !== id) {
        $(this).find('.remove-crx').remove();

        $(this).append(`
          <div
            class="remove-crx"
            id="${id}"
          >
            <img src="${chrome.extension.getURL("icons/trash.png")}" />
          </div>
        `);
   
         document.getElementById(id).onclick = () => promptRemove(title, channel, $(this), id);
      }
      

      if (!items.blocked) return;

      $(this).removeClass('hidden-crx');
      $(this).removeClass('faded-crx');

      for (let i = 0; i < items.blocked.length; i++) {
        if (items.blocked[i].type === 'title') {
          if (title.toLowerCase() === items.blocked[i].term.toLowerCase()) {
            $(this).addClass('hidden-crx');
            
            if (paused) {
              $(this).addClass('faded-crx');
            }
            // $(this).append(items.blocked[i].term);
          }
        } else if (items.blocked[i].type === 'channel') {
          if (channel.toLowerCase() === items.blocked[i].term.toLowerCase()) {
            $(this).addClass('hidden-crx');
            
            if (paused) {
              $(this).addClass('faded-crx');
            }
            // $(this).append(items.blocked[i].term);
          }
        } else if (items.blocked[i].type === 'custom') {
          if (
            title.toLowerCase().indexOf(items.blocked[i].term.toLowerCase()) !== -1
            || channel.toLowerCase() === items.blocked[i].term.toLowerCase()
          ) {
            $(this).addClass('hidden-crx');
            
            if (paused) {
              $(this).addClass('faded-crx');
            }
            // $(this).append(items.blocked[i].term);
          }
        }
      }
    });

    $('#show-crx, #hide-crx').remove();

    let s = $('.hidden-crx').length === 1 ? '' : 's';
    if ($('.hidden-crx').length) {
      if (!paused) {
        $('#related').append(`
          <div id='show-crx'>
            ${$('.hidden-crx').length} recommendation${s} hidden. Click to show.
          </div>
        `);
      } else {
        $('#related').append(`
          <div id='hide-crx'>
            ${$('.hidden-crx').length} recommendation${s} shown. Click to hide.
          </div>
        `);
      }
    } else {
      $('#related').append(`
        <div id='hide-crx'>
          No recommendations hidden.
        </div>
      `);
    }

    document.getElementById('show-crx') && (document.getElementById('show-crx').onclick = () => {
      paused = true;
      cleanup('show');
    });

    document.getElementById('hide-crx') && (document.getElementById('hide-crx').onclick = () => {
      paused = false;
      cleanup('hide');
    });
  });

}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === 'newPage') {
    // alert();
    setTimeout(() => cleanup('first'), 1000);
    // double check
    setTimeout(() => cleanup('secons'), 3000);
  }

  if (request.action === 'update') {
    cleanup('updae');
  }
});