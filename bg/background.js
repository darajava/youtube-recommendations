// chrome.webNavigation.onHistoryStateUpdated.addListener(function(details) {
//     chrome.runtime.sendMessage({action: "newPage"}, () => {})
// });

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){
  if(changeInfo && changeInfo.status == "complete") {
    chrome.tabs.sendMessage(tabId, {action: "newPage"}, function(response) {
    });
  }
});


function onSkuDetails(response) {
  console.log("onSkuDetails", response);
  response = response.response; // :)

  for (let i = 0; i < response.details.inAppProducts.length; i++) {
    chrome.runtime.sendMessage({
      msg: "add_items", 
      data: {
        sku: response.details.inAppProducts[i].sku,
        price: (parseInt(response.details.inAppProducts[i].prices[0].valueMicros) / 1000000),
        currency: response.details.inAppProducts[i].prices[0].currencyCode
      }
    });
  }
}

function onSkuDetailsFailed(response) {
  // doesn't matter
  console.log('hi');
  response = {"response":{"details":{"kind":"chromewebstore#inAppProductList","inAppProducts":[{"kind":"chromewebstore#inAppProduct","sku":"donation","item_id":"fbhkfmepadpddmpmjibpahfmaoofakmn","type":"inapp","state":"ACTIVE","prices":[{"valueMicros":"2000000","currencyCode":"EUR","regionCode":"DE"}],"localeData":[{"title":"Youtube Recommendation Blocker","description":"Youtube Recommendation Blocker is free, this is to help support independent development! :) ","languageCode":"all"}]},{"kind":"chromewebstore#inAppProduct","sku":"donation_recurring","item_id":"fbhkfmepadpddmpmjibpahfmaoofakmn","type":"subs","state":"ACTIVE","prices":[{"valueMicros":"740000","currencyCode":"EUR","regionCode":"DE"}],"localeData":[{"title":"Youtube Recommendation Blocker - Recurring","description":"Recurring support for development of this app. You're great! :) ","languageCode":"all"}]}]}}}

  onSkuDetails(response);
}


function sendBought(response) {
  console.log("onSkuDetails", response);
  response = response.response; // :)

  if (response.details.length) {
    chrome.runtime.sendMessage({
      msg: "has_bought",
    });
  }
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.msg === "request_items") {
      google.payments.inapp.getSkuDetails({
        'parameters': {'env': 'prod'},
        'success': onSkuDetails,
        'failure': onSkuDetailsFailed
      });
    }

    if (request.msg === "buy") {
      google.payments.inapp.buy({
        'parameters': {'env': 'prod'},
        'sku': request.sku,
        'success': () => {},
        'failure': () => {},
      });
    }

    if (request.msg === "check_buy") {
      google.payments.inapp.getPurchases({
        'parameters': {'env': 'prod'},
        'success': sendBought,
        'failure': () => {}
      });
    }
  }
);

